
# Carter Perkins carterp@uoregon.edu
## Description
This is a Python flask webserver implementing PyMOngo, JQuery, Javascript, and AJAX to create an ACP Brevet Control Times Calculator like the one described on www.rusa.org. This server checks implements AJAX in the calculator to get the opening and closing time for the brevets in real time. JQuery and JSON are used to transfer data between the server and the client's webpage, while Python is used for the server itself as well as the underlying logic for the time calculation. PyMongo is used to store brevets in the the database, which are fetched when displaying the results. Also included is a 'Clear' button for dropping the database manually, otherwise when 'Display' is pressed it will retrieve the last submitted contents from the database.
## Running the Server
`sudo docker-compose build`
`sudo docker-compose up`
`localhost:5000`


